//
//  EFBClient.swift
//  Client-840A-Reference
//
//  Created by John Haney on 12/10/17.
//  Copyright © 2017 Lextech. All rights reserved.
//  

import Foundation

fileprivate struct LogEvent {
	let message: String
	let date: Date = Date()
}

public protocol EFBActionHandler {
	func efbLaunched(for: EFBAction)
}

struct FetchQuery<T: Encodable>: Encodable {
	let query: String
	let saveData: T
}

public class EFBClient {
	private static let shared: EFBClient = EFBClient()
	
	public static func add(actionHandler: EFBActionHandler, forBaseURL baseURL: String) {
		shared.handlers[baseURL] = actionHandler
	}
	
	public static func continueUserActivity(_ userActivity: NSUserActivity) -> Bool {
		return shared.handleUserActivity(userActivity)
	}
	
	public static func open(url: URL, options: [UIApplicationOpenURLOptionsKey : Any]) -> Bool {
		return shared.open(url: url, options: options)
	}
	
	public static func send(action: EFBAction) -> Bool {
		guard let message = EFBMessage(action: action)
			else { return false }
		return shared.send(message: message)
	}
	
	public static func save<T: Encodable>(saveData: T) -> Bool {
		let inputParameters = ["saveData": saveData]
		guard let data = try? JSONEncoder().encode(inputParameters)
			else { return false }
		
		let message = EFBMessage(type: .save, payload: data)
		return shared.send(message: message)
	}
	
	public static func back() -> Bool {
		let message = EFBMessage(type: .back, payload: Data())
		return shared.send(message: message)
	}
	
	public static func sync<T: Encodable>(saveData: T) -> Bool {
		let inputParameters: [String: T] = ["saveData": saveData]
		guard let data = try? JSONEncoder().encode(inputParameters)
			else { return false }
		
		let message = EFBMessage(type: .sync, payload: data)
		return shared.send(message: message)
	}

	public static func fetch<T: Encodable>(query: String, saveData: T) -> Bool {
		let inputParameters = FetchQuery(query: query, saveData: saveData)
		guard let data = try? JSONEncoder().encode(inputParameters)
			else { return false }
		
		let message = EFBMessage(type: .fetch, payload: data)
		return shared.send(message: message)
	}
	
	public static func log(message: String) {
		shared.log(event: LogEvent(message: message))
	}
	
	private var hubBaseURL: String {
		didSet {
			UserDefaults.standard.set(hubBaseURL, forKey: "com.lextech.efbclient.EFB.Client.hubBaseURL")
		}
	}
	private var appToken: String {
		didSet {
			UserDefaults.standard.set(appToken, forKey: "com.lextech.efbclient.EFB.Client.appToken")
		}
	}
	
	fileprivate var handlers: [String: EFBActionHandler] = [:]
	
	private init() {
		// load from storage
		hubBaseURL = UserDefaults.standard.string(forKey: "com.lextech.efbclient.EFB.Client.hubBaseURL") ?? "https://multiapp.lextech.com/multiapp/efb/hub/"
		appToken = UserDefaults.standard.string(forKey: "com.lextech.efbclient.EFB.Client.appToken") ?? "ABC123"
	}
	
	fileprivate let logQueue = DispatchQueue(label: "com.lextech.efbclient.EFB.logQueue", qos: .default, attributes: [], autoreleaseFrequency: .workItem, target: nil)
	fileprivate var logs: [LogEvent] = []
	
	private func log(event: LogEvent) {
		logQueue.async {
			self.logs.append(event)
		}
	}
	
	func open(url: URL, options: [UIApplicationOpenURLOptionsKey : Any]) -> Bool {
		guard let components = URLComponents(url: url, resolvingAgainstBaseURL: false)
			else { return false }
		
		var pathOnly = components
		pathOnly.queryItems = nil
		guard let urlString = pathOnly.url?.absoluteString
			else { return false }
		
		let handlersResponding = handlers.filter({ urlString.starts(with: $0.key) })
		
		guard handlersResponding.isEmpty == false
			else { return false }
		
		for handler in handlersResponding {
			let urlString = url.absoluteString
			let actionString = urlString.replacingOccurrences(of: handler.key, with: "", options: .anchored, range: nil)
			if let message = EFBMessage(urlString: actionString, appToken: appToken) {
				switch message.type {
				case let .config(hubBaseURL: hubBaseURL, appToken: appToken):
					print("config: \(hubBaseURL) \(appToken)")
					EFBClient.shared.hubBaseURL = hubBaseURL
					EFBClient.shared.appToken = appToken
					_ = EFBClient.back()
				case let .action(action):
					print("action: \(action)")
					handler.value.efbLaunched(for: action)
				case .back:
					break
				case .fetch:
					break
				case .launch:
					break
				case .save:
					break
				case .sync:
					break
				case .syncResults:
					break
				}
			}
		}
		
		return true
	}
	
	func handleUserActivity(_ userActivity: NSUserActivity) -> Bool {
		guard let url = userActivity.webpageURL,
			let _ = url.query
			else { return false }
		
		let urlString = url.absoluteString
		let handlersResponding = handlers.filter({ urlString.starts(with: $0.key) })
		
		guard handlersResponding.isEmpty == false
			else { return false }
		
		for handler in handlersResponding {
			let action = urlString.replacingOccurrences(of: handler.key, with: "")
			let actionComponents = action.components(separatedBy: "/").filter({ $0.isEmpty == false })
			if let first = actionComponents.first, first == "action" {
				
			} else {
				
			}
		}
		// parse the action
		
		let action = EFBAction.showWeather(route: EFBRoute(waypoints: [EFBWaypoint(station: "MCO", time: Date())]))
		
		handlersResponding.forEach { (_, handler) in
			handler.efbLaunched(for: action)
		}
		
		return true
	}
}

import UIKit

extension EFBClient {
	@discardableResult
	public static func startClient(launchOptions:
		[UIApplicationLaunchOptionsKey: Any]?) -> Bool {
		if let activityInfo = launchOptions?[UIApplicationLaunchOptionsKey.userActivityDictionary] as? NSDictionary,
			let userActivity = activityInfo["UIApplicationLaunchOptionsUserActivityKey"] as? NSUserActivity
		{
			return shared.handleUserActivity(userActivity)
		}
		return false
	}
	
	private func send(message: EFBMessage) -> Bool {
		// include saveData and (modifications to) session
		let partial = message.url
		
		let urlString = "\(hubBaseURL)/\(partial)"
		
		guard let url = URL(string: urlString)
			else { return false }
		
		UIApplication.shared.open(url, options: [:]) { (didSend) in
			print("did send message [\(url)]: \(didSend)")
		}
		
		return true
	}
}
