//
//  EFBHub.swift
//  EFBHub
//
//  Created by John Haney on 12/12/17.
//  Copyright © 2017 Lextech. All rights reserved.
//

import Foundation

struct EFBAppConfig: Codable {
	let appID: AppID
	let appBaseURL: String
	let appToken: String
}

struct EFBActionConfig: Codable {
	let action: EFBActionType
	let appID: AppID
}

struct EFBHubConfig: Codable {
	let hubBaseURL: String
	let apps: [EFBAppConfig]
	let actions: [EFBActionConfig]
}

public class EFBHub {
	private static let shared: EFBHub = EFBHub()
	
	public static func back(appID: AppID) {
		EFBHub.shared.back(appID: appID)
	}
	
	private func back(appID: AppID) {
		if let app = EFBHub.shared.appsByID[appID] {
			back(app: app)
		}
	}
	
	private func back(app: EFBAppConfig) {
		send(message: .back, to: app.appID)
	}

	static func handleActivationWithURL(_ url: URL) -> Bool {
		return shared.handleActivationWithURL(url)
	}
	
	func handleActivationWithURL(_ url: URL) -> Bool {
		guard let components = URLComponents(url: url, resolvingAgainstBaseURL: false)
			else { return false }
		
		var pathOnly = components
		pathOnly.queryItems = nil
		guard pathOnly.url?.absoluteString.starts(with: EFBHub.hubBaseURL) ?? false
			else { return false }
		
		let urlString = url.absoluteString
		let appAndActionString = urlString.replacingOccurrences(of: "\(EFBHub.hubBaseURL)/", with: "", options: .anchored, range: nil)
		let appID = appAndActionString.prefix(while: { $0 != "/" })
		guard let appConfig = EFBHub.shared.appsByID[AppID(appID)]
			else { return false }
		
		let actionString = appAndActionString.replacingOccurrences(of: appConfig.appID, with: "", options: .anchored, range: nil)
		guard let message = EFBMessage(urlString: actionString, appToken: appConfig.appToken)
			else { return false }
		
		return handle(message, from: appConfig)
	}
	
	private static func route(_ action: EFBAction, completion: @escaping (Bool) -> Void) {
		EFBHub.shared.route(action, completion: completion)
	}

	private func route(_ action: EFBAction, completion: @escaping (Bool) -> Void) {
		guard let appID = EFBHub.app(forAction: action.type)
			else {
				// We don't have a handler for this action
				completion(false)
				return
		}
		
		guard let window = (UIApplication.shared.delegate as? AppDelegate)?.window
			else {
				self.send(message: .action(action), to: appID) { (didSend) in
					completion(didSend)
				}
				return
		}
		
		let alert = UIAlertController(title: "Action \(action.type)", message: "Destination appID: \(appID)", preferredStyle: .alert)
		alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
			self.send(message: .action(action), to: appID) { (didSend) in
				completion(didSend)
			}
		}))
		alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { _ in
			completion(false)
		}))
		
		window.rootViewController?.present(alert, animated: true, completion: nil)
		
		print("action (\(action.type)) destination appID: \(appID)")
	}
	
	func handle(_ message: EFBMessage, from: EFBAppConfig) -> Bool {
		// Save any data that came along
		
		switch message.type {
		case .action(let action):
			route(action) { (didRoute) in
				if didRoute == false {
					EFBHub.back(appID: from.appID)
				}
			}
			
			return true
		case .back:
			// TODO: handle this action and then return true
			return false
		case .fetch:
			// TODO: handle this action and then return true
			return false
		case .save:
			// Saved from the payload, so just return back to the app
			back(appID: from.appID)
			return true
		case .sync:
			// TODO: handle this action and then return true
			return false
		case .config,  .launch, .syncResults:
			// These are HUB to App actions, so should not receive these
			return true
		}
	}
	
	func send(message: EFBMessageType, to: AppID) {
		send(message: message, to: to, completion: { _ in })
	}
	
	func send(message: EFBMessageType, to: AppID, completion: @escaping (Bool) -> Void) {
		guard let config = appsByID[to]
			else { return }
		
		let message = EFBMessage(type: message, payload: Data())
		var components = URLComponents(string: "\(config.appBaseURL)/\(message.url)")
		components?.queryItems?.append(URLQueryItem(name: "appToken", value: config.appToken))
		components?.queryItems?.append(URLQueryItem(name: "appData", value: ""))
		
		guard let url = components?.url
			else { return }
		
		UIApplication.shared.open(url, options: [:]) { (didOpen) in
			completion(didOpen)
		}
	}
	
	private var hubBaseURL: String = ""
	private var appForAction: [EFBActionType: AppID] = [:]
	private var appsByID: [AppID: EFBAppConfig] = [:]
	
	private init() {
		loadConfig()
	}
	
	func loadConfig() {
		guard let url = Bundle.main.url(forResource: "EFBHub.hubconfig", withExtension: "plist"),
			let data = try? Data(contentsOf: url)
		else { return }
		
		do {
			let config = try PropertyListDecoder().decode(EFBHubConfig.self, from: data)
			
			hubBaseURL = config.hubBaseURL
			config.apps.forEach({ (appConfig) in
				appsByID[appConfig.appID] = appConfig
			})
			
			config.actions.forEach { (action) in
				appForAction[action.action] = action.appID
			}
		} catch {
			print("error: \(error)")
		}
	}
}

extension EFBHub {
	static func app(forAction action: EFBActionType) -> AppID? {
		return shared.appForAction[action]
	}
	
	static var hubBaseURL: String { return shared.hubBaseURL }
}

import UIKit

extension EFBHub{
	func open(url: URL) {
		UIApplication.shared.open(url, options: [:], completionHandler: nil)
	}
}

