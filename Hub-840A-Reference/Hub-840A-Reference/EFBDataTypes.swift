//
//  EFBDataTypes.swift
//  EFBClient
//
//  Created by John Haney on 12/10/17.
//  Copyright © 2017 Lextech. All rights reserved.
//

import Foundation

public typealias AppID = String
public typealias AppToken = String

public struct EFBWaypoint: Codable {
	let station: String
	let time: Date
	let altitude: Double? // in kilometers
}

public struct EFBRoute: Codable {
	let waypoints: [EFBWaypoint]
}

public enum EFBActionParameters: Codable {
	case dictionary([String: EFBActionParameters])
	case array([EFBActionParameters])
	case value(Data)
	
	enum codingKey: String, CodingKey {
		case type
		case dictionary
		case array
		case value
	}
	
	var string: String {
		let data: Data?
		switch self {
		case .dictionary(let dictionary):
			data = try? JSONEncoder().encode(dictionary)
		case .array(let array):
			data = try? JSONEncoder().encode(array)
		case .value(let value):
			data = value
		}
		
		guard let encodedData = data
			else { return "" }
		
		return String(data: encodedData, encoding: .utf8) ?? ""
	}

	public func encode(to encoder: Encoder) throws {
		var container = encoder.container(keyedBy: codingKey.self)
		switch self {
		case .dictionary(let dictionary):
			try? container.encode(codingKey.dictionary.rawValue, forKey: .type)
			try? container.encode(dictionary, forKey: .dictionary)
		case .array(let array):
			try? container.encode(codingKey.array.rawValue, forKey: .type)
			try? container.encode(array, forKey: .array)
		case .value(let data):
			try? container.encode(codingKey.value.rawValue, forKey: .type)
			try? container.encode(data, forKey: .value)
		}
	}
	
	public init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: codingKey.self)
		let rawType = try container.decode(String.self, forKey: .type)
		let type = codingKey(rawValue: rawType) ?? .type
		switch type {
		case .dictionary:
			self = .dictionary(try container.decode([String: EFBActionParameters].self, forKey: .dictionary))
		case .array:
			self = .array(try container.decode([EFBActionParameters].self, forKey: .array))
		case .value:
			self = .value(try container.decode(Data.self, forKey: .value))
		case .type:
			throw "Invalid encoding"
		}
	}
}

extension String: Error {}

public struct EFBAction {
	public static func showWeather(route: EFBRoute) -> EFBAction {
		let routeData: Data
		if let encodedRoute = try? JSONEncoder().encode(route) {
			routeData = encodedRoute
		} else {
			routeData = Data()
		}
		return EFBAction(type: .showWeather, actionParameters: ["route": .value(routeData)])
	}
	public static func showDocuments() -> EFBAction {
		return EFBAction(type: .showDocs, actionParameters: [:])
	}
	
	public static func showRoutePlanning(route: EFBRoute) -> EFBAction {
		let routeData: Data
		if let encodedRoute = try? JSONEncoder().encode(route) {
			routeData = encodedRoute
		} else {
			routeData = Data()
		}
		return EFBAction(type: .showRoutePlanning, actionParameters: ["route": .value(routeData)])
	}
	
	let type: EFBActionType
	let actionParameters: [String: EFBActionParameters]
}

extension EFBAction {
	internal var urlParameters: [URLQueryItem] {
		guard let data = try? JSONEncoder().encode(actionParameters),
			let string = String(data: data, encoding: .utf8)
			else { return [] }
		
		return [URLQueryItem(name: "actionParameters", value: string)]
	}
	
	internal static func action(type: EFBActionType, urlParameters: [URLQueryItem]) -> EFBAction {
		return EFBAction(type: type, actionParameters: actionParameters(from: urlParameters))
	}
	
	private static func actionParameters(from queryItems: [URLQueryItem]) -> [String: EFBActionParameters] {
		guard let string = queryItems.first(where: { $0.name == "actionParameters" })?.value,
			let data = string.data(using: .utf8),
			let result = try? JSONDecoder().decode([String: EFBActionParameters].self, from: data)
			else { return [:] }
		
		return result
	}
}

enum EFBActionType: String, Codable {
	case showWeather
	case showDocs
	case showRoutePlanning

	static var all: [EFBActionType] { return [.showWeather, .showDocs, .showRoutePlanning] }
	static var count: Int { return all.count }
}

public enum EFBMessageType {
	case action(EFBAction)
	
	case back
	case save
	case sync
	case fetch
	
	case launch
	case syncResults
	case config(hubBaseURL: String, appToken: String)
}

fileprivate extension EFBMessageType {
	var urlPath: String {
		switch self {
		case .action(let action):
			return "/action/\(action.type.rawValue)/"
		case .back:
			return "back"
		case .save:
			return "save"
		case .sync:
			return "sync"
		case .fetch:
			return "fetch"
		case .launch:
			return "launch"
		case .syncResults:
			return "syncResults"
		case .config:
			return "config"
		}
	}
	var urlParameters: [URLQueryItem] {
		switch self {
		case .action(let action):
			return action.urlParameters
		case .back:
			return []
		case .save:
			return []
		case .sync:
			return []
		case .fetch:
			return []
		case .launch:
			return []
		case .syncResults:
			return []
		case let .config(hubBaseURL, appToken):
			return [URLQueryItem(name: "hubBaseURL", value: hubBaseURL),
					URLQueryItem(name: "appToken", value: appToken)]
		}
	}
}

public struct EFBMessage {
	let type: EFBMessageType
	let payload: Data
	
	internal var url: String {
		let payload64 = payload.base64EncodedString()
		var components = URLComponents()
		var queryItems: [URLQueryItem] = [URLQueryItem(name: "payload", value: payload64)]
		queryItems.append(contentsOf: type.urlParameters)
		
		components.path = type.urlPath
		components.queryItems = queryItems
		return components.url?.absoluteString ?? ""
	}

	internal init(type: EFBMessageType, payload: Data) {
		self.type = type
		self.payload = payload
	}
	// arinc-840a-hub-reference:/FDPro/action/showWeather
	init?(urlString: String, appToken: String) {
		guard let url = URL(string: "scheme:\(urlString)"),
			let urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false)
			else { return nil }
		let pathString: String
		if let host = urlComponents.host {
			pathString = "\(host)\(urlComponents.path)"
		} else {
			pathString = urlComponents.path
		}
		let path = pathString.split(separator: "/", maxSplits: 10000, omittingEmptySubsequences: true)
		guard let verb = path.first
			else { return nil }
		
		if let payload64 = urlComponents.queryItems?.first(where: { $0.name == "payload" })?.value,
			let payloadData = Data(base64Encoded: payload64) {
			self.payload = payloadData
		} else {
			self.payload = Data()
		}
		
		switch verb {
		case "action":
			guard path.count > 1,
				let actionType = EFBActionType(rawValue: String(path[1]))
				else { return nil }
			let action = EFBAction.action(type: actionType, urlParameters: urlComponents.queryItems ?? [])
			self.type = .action(action)
		case "config":
			guard let hubBaseURL = urlComponents.queryItems?.first(where: { $0.name == "hubBaseURL" })?.value,
				let appToken = urlComponents.queryItems?.first(where: { $0.name == "appToken" })?.value
				else { return nil }
			
			self.type = .config(hubBaseURL: hubBaseURL, appToken: appToken)
		case "launch":
			self.type = .launch
		case "back":
			self.type = .back
		case "save":
			self.type = .save
		default:
			print("ERROR: unsupported EFB 840A verb: \(verb)")
			return nil
		}
	}
}

public extension EFBMessage {
    init?(action: EFBAction) {
        guard let data = try? JSONEncoder().encode(action.actionParameters)
            else { return nil }
        self.init(type: .action(action), payload: data)
    }
}

// App to Hub
public extension EFBMessage {
	public static func back() -> EFBMessage {
        return EFBMessage(type: .back, payload: Data())
	}
	
	public static func save() -> EFBMessage {
        return EFBMessage(type: .save, payload: Data())
	}
	
	public static func sync() -> EFBMessage {
		return EFBMessage(type: .sync, payload: Data())
	}
	
	public static func fetch() -> EFBMessage {
		return EFBMessage(type: .fetch, payload: Data())
	}
}

// Hub to App
public extension EFBMessage {
	public static func launch() -> EFBMessage {
		return EFBMessage(type: .launch, payload: Data())
	}
	
	public static func syncResults() -> EFBMessage {
		return EFBMessage(type: .syncResults, payload: Data())
	}
	
    public static func config(hubBaseURL: String, appToken: String) -> EFBMessage {
		return EFBMessage(type: .config(hubBaseURL: hubBaseURL, appToken: appToken), payload: Data())
	}
}

