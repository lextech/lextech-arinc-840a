//
//  AppDelegate.swift
//  Hub-840A-Reference
//
//  Created by John Haney (Lextech) on 5/4/18.
//  Copyright © 2018 Lextech Global Services. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

	var window: UIWindow?

	func handleUserActivity(_ userActivity: NSUserActivity) -> Bool {
		if let url = userActivity.webpageURL,
			EFBHub.handleActivationWithURL(url) {
			return true
		}
		
		// Otherwise do custom app handling of URL activations
		
		return true
	}

	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
		// Override point for customization after application launch.
		
		if let activityInfo = launchOptions?[UIApplicationLaunchOptionsKey.userActivityDictionary] as? NSDictionary,
			let userActivity = activityInfo["UIApplicationLaunchOptionsUserActivityKey"] as? NSUserActivity
		{
			return handleUserActivity(userActivity)
		}
		
		if let url = launchOptions?[UIApplicationLaunchOptionsKey.url] as? URL
		{
			return EFBHub.handleActivationWithURL(url)
		}
		
		return true
	}

	func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
		return handleUserActivity(userActivity)
	}

	func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
		if EFBHub.handleActivationWithURL(url) {
			return true
		}
		
		return true
	}
}

